package com.renseki.app.projectpertamaku

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.renseki.app.projectpertamaku.model.Mahasiswa
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class FourthActivity : AppCompatActivity(), StudentInputFragment.StudentInputActionListener {

    private lateinit var recentStudentFragment: RecentStudentFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fourth_activity)

        tempelFragment1()
        tempelFragment2()

        tryBackgroundProcesses()
    }

    data class CobaSaja(
            val judul: String
    )

    private fun tryBackgroundProcesses() {
        // Kirim broadcast
        val data = CobaSaja("sebuah data")
        EventBus.getDefault().post(data)

        // Jalankan proses di belakang layar
        val bisaDisubscribe = Observable.create<String> { emitter ->
            (0 until 10).forEach { angka ->
                emitter.onNext(angka.toString())
            }
        }

        bisaDisubscribe
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<String> {
                    override fun onComplete() { // ini kalau tuntas

                    }

                    override fun onSubscribe(d: Disposable) { // saat pertama kali subscribe

                    }

                    override fun onNext(t: String) { // tiap kali terima data
                        Log.d("RX", t)
                    }

                    override fun onError(e: Throwable) { // kalau segmen program throw Exception
                        Log.e("RX", e.message ?: "")
                    }
                })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun sembarang(data: CobaSaja) {
        Toast
                .makeText(
                        this,
                        data.judul,
                        Toast.LENGTH_LONG
                )
                .show()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun tempelFragment2() {
        recentStudentFragment = RecentStudentFragment.newInstance()

        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.fragment_container_2,
                    recentStudentFragment
            )
            commit()
        }
    }

    private fun tempelFragment1() {
        val fragment = StudentInputFragment.newInstance(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }

    override fun onMahasiswaReady(mahasiswa: Mahasiswa) {
        Toast
                .makeText(
                        this,
                        mahasiswa.name,
                        Toast.LENGTH_LONG
                )
                .show()
        recentStudentFragment.receiveStudent(mahasiswa)
    }
}
